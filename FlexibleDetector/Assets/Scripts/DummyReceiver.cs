using UnityEngine;

public class DummyReceiver : MonoBehaviour
{
    public void WhenTriggerEnter(Collider other)
    {
        Debug.Log($"WhenTriggerEnter");
    }

    public void WhenTriggerExit(Collider other)
    {
        Debug.Log($"WhenTriggerExit");
    }
}