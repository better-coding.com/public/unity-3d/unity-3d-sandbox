using UnityEngine;

public class MultiAreaDetector : MonoBehaviour
{
    public void WhenTopTriggerEnter(Collider collider)
    {
        Debug.Log("WhenTopTriggerEnter");
    }

    public void WhenTopTriggerExit(Collider collider)
    {
        Debug.Log("WhenTopTriggerExit");
    }

    public void WhenBottomTriggerEnter(Collider collider)
    {
        Debug.Log("WhenBottomTriggerEnter");
    }

    public void WhenBottomTriggerExit(Collider collider)
    {
        Debug.Log("WhenBottomTriggerExit");
    }
}