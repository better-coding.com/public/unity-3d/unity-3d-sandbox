using UnityEngine;

namespace DefaultNamespace
{
    public class MultiAreaDetectorV2: MonoBehaviour
    {
        [SerializeField] private Detector topAreaDetector;
        [SerializeField] private Detector bottomAreaDetector;

        private void OnEnable()
        {
            topAreaDetector.onTriggerEnter.AddListener(WhenTopTriggerEnter);
            topAreaDetector.onTriggerExit.AddListener(WhenTopTriggerExit);
            bottomAreaDetector.onTriggerEnter.AddListener(WhenBottomTriggerEnter);
            bottomAreaDetector.onTriggerExit.AddListener(WhenBottomTriggerExit);
        }
        
        private void OnDisable()
        {
            topAreaDetector.onTriggerEnter.RemoveListener(WhenTopTriggerEnter);
            topAreaDetector.onTriggerExit.RemoveListener(WhenTopTriggerExit);
            bottomAreaDetector.onTriggerEnter.RemoveListener(WhenBottomTriggerEnter);
            bottomAreaDetector.onTriggerExit.RemoveListener(WhenBottomTriggerExit);
        }
        
        
        public void WhenTopTriggerEnter(Collider collider)
        {
            Debug.Log("WhenTopTriggerEnter");
        }

        public void WhenTopTriggerExit(Collider collider)
        {
            Debug.Log("WhenTopTriggerExit");
        }

        public void WhenBottomTriggerEnter(Collider collider)
        {
            Debug.Log("WhenBottomTriggerEnter");
        }

        public void WhenBottomTriggerExit(Collider collider)
        {
            Debug.Log("WhenBottomTriggerExit");
        }
    }
}