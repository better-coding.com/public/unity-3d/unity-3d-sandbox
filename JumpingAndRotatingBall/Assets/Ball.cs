using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour
{
    public GameObject jumpAnimator;
    public GameObject rotationAnimator;

    private bool rolling;
    private Vector3 target;

    private float speed = 10f;
    private float radius = 1f;


    private System.Random random = new System.Random();
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            jumpAnimator.GetComponent<Animator>().SetBool("jumping", true);
        }
        else if (Input.GetKeyUp(KeyCode.Space))
        {
            jumpAnimator.GetComponent<Animator>().SetBool("jumping", false);
        }

        if (Input.GetKeyDown(KeyCode.Return))
        {
            this.RollTo(new Vector3(random.Next(-5, 5), 0f, random.Next(-5, 5)));
        }


        if (rolling)
        {
            Vector3 vectorToTarget = this.target - this.transform.position;
            float distanceTotheTarget = vectorToTarget.magnitude;
            float distanceIntheCurrentFrame = speed * Time.deltaTime;
            if (distanceTotheTarget > 0)
            {
                //Move
                this.transform.position = Vector3.MoveTowards(this.transform.position, target, distanceIntheCurrentFrame);

                //Rolling ball effect
                float circumeference = 2.0f * Mathf.PI * this.radius;
                float angleDetla = distanceIntheCurrentFrame / circumeference * 360;
                Vector3 rotationAxix = Quaternion.LookRotation(vectorToTarget) * Vector3.right;
                this.rotationAnimator.transform.Rotate(rotationAxix, angleDetla, Space.World);
            }
            else
            {
                //The target was reached
                this.rolling = false;
            }
        }
    }

    private void RollTo(Vector3 target)
    {
        this.target = target;
        this.rolling = true;
    }
}
