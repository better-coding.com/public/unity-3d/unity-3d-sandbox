using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RotatingBall : MonoBehaviour
{

    [SerializeField] private Slider _slider;
    [SerializeField] private Toggle _toggle;

    [SerializeField] private float _maxSpeed;

        private Renderer _renderer;

    // Start is called before the first frame update
    void Start()
    {
        _renderer = transform.GetComponent<Renderer>();
    }

    // Update is called once per frame
    void Update()
    {
        transform.GetComponent<Rigidbody>().maxAngularVelocity = _maxSpeed;
        transform.GetComponent<Rigidbody>().angularVelocity = Vector3.one * _slider.value * _maxSpeed;

        float blurFactor = _toggle.isOn ? Mathf.Min(Mathf.Abs(transform.GetComponent<Rigidbody>().angularVelocity.magnitude) / 100f*4f, 1f) : 0f;
        MaterialPropertyBlock _props = new MaterialPropertyBlock();
        _renderer.GetPropertyBlock(_props);
        _props.SetFloat("_blurFactor", blurFactor);
        _renderer.SetPropertyBlock(_props);
    }
}
