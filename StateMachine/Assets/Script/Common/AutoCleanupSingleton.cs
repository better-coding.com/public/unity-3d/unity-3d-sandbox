using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LazyInstantiatedSingleton<T> : MonoBehaviour where T : MonoBehaviour
{
    private static T _instance;

    public static T Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<T>();
                if (_instance == null)
                {
                    var gameObject = new GameObject("Singleton of: " + typeof(T));
                    _instance = gameObject.AddComponent<T>();
                }
            }
            return _instance;
        }
    }

    private void Awake()
    {
        if (_instance != null)
        {
            Destroy(this.gameObject);
        }
    }

}
