using System;
using System.Collections.Generic;
using UnityEngine;

public class StateMachine
{
    private IState _currentState;

    private static List<Transition> EmptyTransitions = new List<Transition>();
    private Dictionary<Type, List<Transition>> _transitions = new Dictionary<Type, List<Transition>>();

    private List<Transition> _currentTransitions = EmptyTransitions;
    private List<Transition> _anyTransitions = new List<Transition>();


    public void Tick()
    {
        var transition = GetTransition();
        if (transition != null)
        {
            SetState(transition.To);
        }

        _currentState?.Tick();
    }

    public void AddTransition(IState from, IState to, Func<bool> predicate)
    {
        if (!_transitions.TryGetValue(from.GetType(), out var transitions))
        {
            transitions = new List<Transition>();
            _transitions[from.GetType()] = transitions;
        }
        transitions.Add(new Transition(to, predicate));
    }

    public void AddAnyTransitions(IState to, Func<bool> predicate)
    {
        _anyTransitions.Add(new Transition(to, predicate));
    }

    public void SetState(IState state)
    {
        Debug.Log($"SetState: {state}");
        if (state == _currentState)
        {
            return;
        }

        _currentState?.OnExit();
        _currentState = state;


        if (!_transitions.TryGetValue(_currentState.GetType(), out _currentTransitions))
        {
            _currentTransitions = EmptyTransitions;
        }

        _currentState.OnEnter();
    }

    private Transition GetTransition()
    {
        foreach (var transition in _anyTransitions)
        {
            if (transition.Condition())
            {
                return transition;
            }
        }

        foreach (var transition in _currentTransitions)
        {
            if (transition.Condition())
            {
                return transition;
            }
        }

        return null;
    }
}

internal class Transition
{
    public Func<bool> Condition { get; }
    public IState To { get; }

    public Transition(IState to, Func<bool> condition)
    {
        To = to;
        Condition = condition;
    }
}

public interface IState
{
    void Tick();
    void OnEnter();
    void OnExit();
}