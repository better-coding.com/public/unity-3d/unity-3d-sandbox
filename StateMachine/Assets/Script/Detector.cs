using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;

public class Detector : MonoBehaviour
{
    private List<GameObject> _detectedItems = new List<GameObject>();

    private void OnCollisionEnter(Collision other)
    {
        _detectedItems.Add(other.gameObject);
    }

    private void OnCollisionExit(Collision other)
    {
        _detectedItems.Remove(other.gameObject);
    }

    public GameObject GetDetectedObjectOfType(String tag) {
        return _detectedItems.Find( i => i.gameObject.tag.Equals(tag));
    }
}
