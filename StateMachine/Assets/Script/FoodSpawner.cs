using System.Collections.Generic;
using UnityEngine;

public class FoodSpawner : MonoBehaviour
{
    private const float SPAWN_RADUIS = GameManager.GAME_AREA_RADIUS;
    public static FoodSpawner Instance { get; private set; }
    private static System.Random _random = new System.Random();
    const int MAX_FOOD_COUNT = 10;

    public GameObject _foodPrefab;

    private GameObject[] _foodCollection = new GameObject[MAX_FOOD_COUNT];

    void Awake()
    {
        Instance = this;

        for (var i = 0; i < MAX_FOOD_COUNT; i++)
        {
            var food = Instantiate(_foodPrefab, Vector3.zero, Quaternion.identity);
            food.SetActive(false);
            _foodCollection[i] = food;

        }
    }

    // Update is called once per frame
    void Update()
    {
        foreach (var food in _foodCollection)
        {
            if (!food.activeSelf)
            {
                spawnFood(food);
            }
        }
    }

    private void spawnFood(GameObject food)
    {
        food.transform.position = new Vector3(-SPAWN_RADUIS + (float)_random.NextDouble() * SPAWN_RADUIS * 2f, 0.5f, -SPAWN_RADUIS + (float)_random.NextDouble() * SPAWN_RADUIS * 2f);
        food.SetActive(true);
    }
}
