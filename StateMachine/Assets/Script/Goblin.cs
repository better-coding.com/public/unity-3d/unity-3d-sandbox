using System;
using System.Collections;
using System.Collections.Generic;
using State;
using UnityEngine;
using UnityEngine.AI;

public class Goblin : MonoBehaviour
{
    public Material _maleMaterial;
    public Material _femaleMaterial;

    public bool _male;

    public float Health { get; set; }

    private Transform _goblinBase;

    private Animator _animator;

    private NavMeshAgent _navMeshAgent;
    private StateMachine _stateMachine;
    private Detector _detector;



    public GameObject Target { get; set; }
    void Awake()
    {
        Health = 40f;
        _goblinBase = transform.Find("GOBLIN_BASE");
        _animator = GetComponent<Animator>();
        _navMeshAgent = GetComponent<NavMeshAgent>();
        _detector = transform.Find("Detector").GetComponent<Detector>();

        _stateMachine = new StateMachine();

        var idle = new Idle(this);
        var walking = new Walking(this, this._animator, _navMeshAgent, transform);
        var searchForAnyTarget = new SearchForTarget(this, _detector, null);
        var searchForFood = new SearchForTarget(this, _detector, "Food");
        var operatingOnTarget = new OperatingOnTarget(this, this._animator);


        _stateMachine.AddTransition(idle, searchForAnyTarget, HasNoTarget());
        _stateMachine.AddTransition(searchForAnyTarget, idle, HasNoTarget());
        _stateMachine.AddTransition(searchForAnyTarget, walking, HasTarget());
        _stateMachine.AddTransition(walking, operatingOnTarget, ReachedTarget());
        _stateMachine.AddTransition(walking, searchForFood, IsHungry());
        _stateMachine.AddTransition(searchForFood, walking, HasTarget());
        _stateMachine.AddTransition(operatingOnTarget, idle, HasNoTarget());

        // _stateMachine.AddAnyTransitions(die, IsDead());

        // _stateMachine.AddTransition(idle, searchForAnyTarget, HasNoTarget());
        // _stateMachine.AddTransition(searchForAnyTarget, idle, HasNoTarget());
        // _stateMachine.AddTransition(searchForAnyTarget, walking, HasTarget());
        // _stateMachine.AddTransition(walking, operatingOnTarget, ReachedTarget());
        // _stateMachine.AddTransition(operatingOnTarget, idle, HasNoTarget());

        Func<bool> HasTarget() => () => this.Target != null;
        Func<bool> HasNoTarget() => () => this.Target == null;
        Func<bool> ReachedTarget() => () => Vector3.Distance(this.Target.transform.position, this.transform.position) < 0.1f;
        Func<bool> IsHungry() => () => Health < 30f;




        _stateMachine.SetState(idle);
    }

    void Start()
    {
        _goblinBase.GetComponent<Renderer>().material = (_male ? _maleMaterial : _femaleMaterial);
    }

    // Update is called once per frame
    void Update()
    {
        Health -= Time.deltaTime;
        _stateMachine.Tick();
    }
}
