using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoblinSpawner : MonoBehaviour
{
    private const float SPAWN_RADUIS = GameManager.GAME_AREA_RADIUS;

    private const int INITIAL_GOBLINS = 10;
    private static System.Random _random = new System.Random();
    public GoblinSpawner Instance { get; private set; }

    public GameObject _goblinPrefab;

    void Awake()
    {
        Instance = this;
        for (var i = 0; i < INITIAL_GOBLINS; i++)
        {
            SpawnAtRandomPlace();
        }
    }

    void Start()
    {

    }

    public void SpawnAtRandomPlace()
    {
        Vector3 position = new Vector3(-SPAWN_RADUIS + (float)_random.NextDouble() * SPAWN_RADUIS * 2f, 0.0f, -SPAWN_RADUIS + (float)_random.NextDouble() * SPAWN_RADUIS * 2f);
        SpawnAt(position);
    }

    public void SpawnAt(Vector3 position)
    {
        var goblin = Instantiate(_goblinPrefab, position, Quaternion.identity);
        goblin.GetComponent<Goblin>()._male = (_random.NextDouble() < .5f);
    }

    // Update is called once per frame
    void Update()
    {

    }
}
