using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitDetector : MonoBehaviour
{
    public GameObject _hitPS;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other) {
        Debug.Log("Hit!");
        GameObject ps = Instantiate(_hitPS, other.transform.position, Quaternion.identity);
        Destroy(ps, 1f);
    }
}
