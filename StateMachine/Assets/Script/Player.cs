using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public GameObject _ballPrefab;


    private GameObject _currentBall;

    private void Awake()
    {
        SwipeDetector.OnSwipeUpStart += SwipeDetector_OnSwipeUpStart;
        SwipeDetector.OnSwipeUpEnd += SwipeDetector_OnSwipeUpEnd;
    }



    private void SwipeDetector_OnSwipeUpStart(Vector2 v)
    {
        if (_currentBall != null) {
            Destroy(_currentBall, 2f);
        }
        // new Vector3(0.63f, 3.53f, -6.64f)
        _currentBall = Instantiate(this._ballPrefab, this.transform.position + new Vector3(0f, 1.5f,0f), Quaternion.identity);
        _currentBall.GetComponent<Rigidbody>().isKinematic = true;
    }

    private void SwipeDetector_OnSwipeUpEnd(Vector2 v)
    {

         _currentBall.GetComponent<Rigidbody>().isKinematic = false;
        float force = 4f + v.y/Screen.height * 5f;
        _currentBall.GetComponent<Rigidbody>().AddForce(new Vector3(0f, 1.5f, 1f).normalized * force, ForceMode.Impulse);
        _currentBall.GetComponent<Rigidbody>().AddTorque(new Vector3(1.0f, 0.5f, 0.2f));

        // _currentBall.GetComponent<Rigidbody>().isKinematic = false;
        // float force = v.y/Screen.height * 20f;
        // _currentBall.GetComponent<Rigidbody>().AddForce(new Vector3(0f, 1.5f, 1f).normalized * force, ForceMode.Impulse);
        // _currentBall.GetComponent<Rigidbody>().AddTorque(new Vector3(1.0f, 0.5f, 0.2f));
    }

    private void OnDestroy()
    {
        SwipeDetector.OnSwipeUpStart -= SwipeDetector_OnSwipeUpStart;
        SwipeDetector.OnSwipeUpEnd -= SwipeDetector_OnSwipeUpEnd;
    }
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
}
