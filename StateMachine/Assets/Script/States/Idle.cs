using UnityEngine;
using UnityEngine.AI;

namespace State
{
    public class Idle : IState
    {
        private Goblin _goblin;

        public Idle(Goblin goblin)
        {
            this._goblin = goblin;
        }

        public void OnEnter()
        {
        }

        public void OnExit()
        {
        }

        public void Tick()
        {
             
        }
    }
}
