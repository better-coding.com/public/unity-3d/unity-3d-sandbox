using UnityEngine;
using UnityEngine.AI;


namespace State
{
    public class OperatingOnTarget : IState
    {
        private Goblin _goblin;
        private Animator _animator;


        private float targetOperationTime;
        private const float MAX_TIME_SPENT = 2f;

        public OperatingOnTarget(Goblin goblin, Animator animator)
        {
            this._goblin = goblin;
            this._animator = animator;
        }

        public void OnEnter()
        {
            _animator.SetInteger("moving", 99);
            targetOperationTime = 0f;
        }

        public void OnExit()
        {
            _animator.SetInteger("battle", 0);
            _animator.SetInteger("moving", 0);
        }

        public void Tick()
        {
            Debug.Log("OperatingOnTarget tick");
            targetOperationTime += Time.deltaTime;
            if (targetOperationTime > MAX_TIME_SPENT)
            {
                _goblin.Target = null;
            }
        }
    }
}