
using System;
using UnityEngine;

namespace State
{
    public class SearchForTarget : IState
    {
        private Goblin _goblin;
        private Detector _detector;
        private String _targetTag;

        private GameObject _targetIndicator;

        public SearchForTarget(Goblin goblin, Detector detector, String targetTag)
        {
            this._goblin = goblin;
            this._detector = detector;
            this._targetTag = targetTag;
        }

        public void OnEnter()
        {
            if (_targetIndicator == null) {
                _targetIndicator = new GameObject("Target Indicator");
            }
        }

        public void OnExit()
        {
        }

        public void Tick()
        { 
            GameObject choosenTarget = _goblin.Target;
            
            if (_targetTag == null)
            {
                Vector3 position = new Vector3(UnityEngine.Random.Range(-GameManager.GAME_AREA_RADIUS, GameManager.GAME_AREA_RADIUS), -10, UnityEngine.Random.Range(-GameManager.GAME_AREA_RADIUS, GameManager.GAME_AREA_RADIUS));
                if (Physics.Raycast(position + new Vector3(0, 200.0f, 0), Vector3.down, out var hit, 200.0f))
                {
                    Debug.Log($"1 new Target: {hit.point}");
                    _targetIndicator.transform.position = hit.point;
                    choosenTarget = _targetIndicator;
                }
                else
                {
                    Debug.Log("there seems to be no ground at this position");
                }
            }
            else
            {
                var targetGo = _detector.GetDetectedObjectOfType(_targetTag);
                if (targetGo != null)
                {
                    Debug.Log($"2 new Target: {targetGo.transform.position}");
                    choosenTarget = targetGo;
                }
            }

            _goblin.Target =choosenTarget;

        }
    }
}