using UnityEngine;
using UnityEngine.AI;


namespace State
{
    public class Walking : IState
    {
        private Goblin _goblin;
        private Animator _animator;

        private Transform _transform;

        private NavMeshAgent _navMeshAgent;

        private Vector3 _moveDirection = Vector3.zero;

        private float _originalSpeed;

        public Walking(Goblin goblin, Animator animator, NavMeshAgent navMeshAgent, Transform transform)
        {
            this._goblin = goblin;
            this._animator = animator;
            this._navMeshAgent = navMeshAgent;
            this._transform = transform;
        }

        public void OnEnter()
        {
            _originalSpeed = _animator.speed;
            _animator.speed = _navMeshAgent.speed/2f;
            _animator.SetInteger("moving", 1);
        }

        public void OnExit()
        {
            _animator.speed = _originalSpeed;
            _navMeshAgent.ResetPath();
            _animator.SetInteger("moving", 0);
        }

        public void Tick()
        {
            _navMeshAgent.SetDestination(_goblin.Target.transform.position);
            // _navMeshAgent.Move()
            // if (_controller.isGrounded)
            // {
            //     _moveDirection = _transform.forward * _speed;
            // }
            // // float turn = Input.GetAxis("Horizontal");
            // // transform.Rotate(0, turn * turnSpeed * Time.deltaTime, 0);
            // _controller.Move(_moveDirection * Time.deltaTime);
            // _moveDirection.y -= _gravity * Time.deltaTime;
        }
    }
}