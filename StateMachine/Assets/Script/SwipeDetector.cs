using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwipeDetector : MonoBehaviour
{
    public static event Action<Vector2> OnSwipeUpStart;
    public static event Action<Vector2> OnSwipeUpMove;
    public static event Action<Vector2> OnSwipeUpEnd;
    private Vector2 fingerStart;
    private Vector2 fingerDown;
    private Vector2 fingerUp;
    public bool detectSwipeOnlyAfterRelease = false;

    public float SWIPE_THRESHOLD = 2f;

    // Update is called once per frame
    void Update()
    {

        foreach (Touch touch in Input.touches)
        {
            if (touch.phase == TouchPhase.Began)
            {
                fingerUp = touch.position;
                fingerDown = touch.position;
                fingerStart = touch.position;
                OnSwipeUpStart?.Invoke(Vector2.zero);
            }

            //Detects Swipe while finger is still moving
            if (touch.phase == TouchPhase.Moved)
            {
                if (!detectSwipeOnlyAfterRelease)
                {
                    fingerDown = touch.position;
                    checkSwipe(false);
                }
            }

            //Detects swipe after finger is released
            if (touch.phase == TouchPhase.Ended)
            {
                fingerDown = touch.position;
                checkSwipe(true);
            }
        }
    }

    void checkSwipe(bool released)
    {
        if (released)
        {
            if (fingerDown.y - fingerUp.y > 0)//up swipe
            {
                OnSwipeUpEnd?.Invoke(new Vector2(fingerDown.x - fingerStart.x, fingerDown.y - fingerStart.y));
            }
            else
            {
                //down
            }
        }
        else
        {
            //Check if Vertical swipe
            if (verticalMove() > SWIPE_THRESHOLD && verticalMove() > horizontalValMove())
            {
                //Debug.Log("Vertical");
                if (fingerDown.y - fingerUp.y > 0)//up swipe
                {
                    OnSwipeUpMove?.Invoke(new Vector2(fingerDown.x - fingerStart.x, fingerDown.y - fingerStart.y));
                }
                else if (fingerDown.y - fingerUp.y < 0)//Down swipe
                {
                    OnSwipeDown();
                }
                fingerUp = fingerDown;
            }

            //Check if Horizontal swipe
            else if (horizontalValMove() > SWIPE_THRESHOLD && horizontalValMove() > verticalMove())
            {
                //Debug.Log("Horizontal");
                if (fingerDown.x - fingerUp.x > 0)//Right swipe
                {
                    OnSwipeRight();
                }
                else if (fingerDown.x - fingerUp.x < 0)//Left swipe
                {
                    OnSwipeLeft();
                }
                fingerUp = fingerDown;
            }

            //No Movement at-all
            else
            {
                //Debug.Log("No Swipe!");
            }
        }

    }

    float verticalMove()
    {
        return Mathf.Abs(fingerDown.y - fingerUp.y);
    }

    float horizontalValMove()
    {
        return Mathf.Abs(fingerDown.x - fingerUp.x);
    }

    //////////////////////////////////CALLBACK FUNCTIONS/////////////////////////////
    void OnSwipeUp(float value)
    {
        Debug.Log($"Swipe UP: ${value}");
    }

    void OnSwipeUpReleased(float value)
    {
        Debug.Log($"Swipe UP Released: ${value}");
    }

    void OnSwipeDown()
    {
        // Debug.Log("Swipe Down");
    }

    void OnSwipeLeft()
    {
        // Debug.Log("Swipe Left");
    }

    void OnSwipeRight()
    {
        // Debug.Log("Swipe Right");
    }
}

