using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

public class damage_calculator
{
    // A Test behaves as an ordinary method
    [Test]
    public void sets_damage_to_half_with_50_percent_mitigation()
    {
        int finalDamage = DamageCalculator.CalculateDamage(10, 0.5f);
        Assert.AreEqual(5, finalDamage);
    }

    [Test]
    public void sets_damage_to_half_with_80_percent_mitigation()
    {
        int finalDamage = DamageCalculator.CalculateDamage(10, 0.8f);
        Assert.AreEqual(2, finalDamage);
    }

    // // A UnityTest behaves like a coroutine in Play Mode. In Edit Mode you can use
    // // `yield return null;` to skip a frame.
    // [UnityTest]
    // public IEnumerator damage_calculatorWithEnumeratorPasses()
    // {
    //     // Use the Assert class to test conditions.
    //     // Use yield to skip a frame.
    //     yield return null;
    // }
}
